package com.plustitsolution.zeenshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.plustitsolution.zeenshop.domain.BuyDomain;
import com.plustitsolution.zeenshop.domain.ProductDomain;
import com.plustitsolution.zeenshop.service.ZeenshopService;

@RestController
public class ZeenshopController {

	@Autowired
	private ZeenshopService service;
	
	@PostMapping("/addProduct")
	public void addProduct(@RequestParam("productID") String productID,@RequestParam("price") Integer price, @RequestParam("stock") Integer stock) {
		service.addProduct( productID, price, stock);
	}
	
	@GetMapping("/listallProduct")
    public List<ProductDomain> listallProduct(){
        return service.listallProduct();
    }
	
	@PutMapping("/buyProduct/{productID}")
	public BuyDomain buyProduct(@PathVariable("productID") String productID,@RequestParam("amount")  int amount, @RequestParam("quantity") int quantity) {
		return service.buyProduct(productID, amount, quantity);
	}
	
	@GetMapping("/history")
    public List<BuyDomain> history(){
        return service.history();
    }
	
}

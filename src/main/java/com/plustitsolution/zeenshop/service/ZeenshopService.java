package com.plustitsolution.zeenshop.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.springframework.stereotype.Service;

import com.plustitsolution.zeenshop.domain.BuyDomain;
import com.plustitsolution.zeenshop.domain.ProductDomain;

@Service
public class ZeenshopService {
//
//	public static void main(String[] args) {
//		addProduct("P1", 20, 100);
//		addProduct("P1", 20, 1000);
//		addProduct("P2", 20, 1000);
//		listallProduct();
//
//		buyProduct("P1", 100, 1);
//		buyProduct("P1", 100, 1);
//		buyProduct("P1", 100, 1);
//		buyProduct("P1", 100, 1);
////		System.out.println(BUY_MAP);
//		history();
//		listallProduct();
//	}

	private static Map<String, ProductDomain> PRODUCT_MAP = new HashMap<>();

	private static Map<String, BuyDomain> BUY_MAP = new TreeMap<>();
	private static int buycount = 0;

	public void addProduct(String productID, Integer price, Integer stock) {

		ProductDomain product = new ProductDomain(productID, price, stock);

		if (PRODUCT_MAP.containsKey(productID)) {
			ProductDomain upProduct = PRODUCT_MAP.get(productID);
			int upDate = upProduct.getStock() + stock;
			product.setStock(upDate);
		}
		PRODUCT_MAP.put(productID, product);
	}

//	public static void listallProduct() {
//		System.out.println();
//		System.out.println("------ All of the Product in stock ------");
//		for (Entry<String, ProductDomain> entry : PRODUCT_MAP.entrySet()) {
//			System.out.println(entry.getKey() + " " + entry.getValue());
//		}
//	}

	public  List<ProductDomain> listallProduct() {
		List<ProductDomain> list = new ArrayList<>();
		PRODUCT_MAP.entrySet().stream().forEach(domain -> {
			list.add(domain.getValue());
		});
		return list;
	}

	public BuyDomain buyProduct(String productID, int amount, int quantity) {

		ProductDomain product = PRODUCT_MAP.get(productID);
		String buyID = "B" + ++buycount;
		// เช็คว่ามีของหรือไม่
		if (product != null) {
			int total = quantity * product.getPrice();
			int change = amount - total;
			
			if (amount >= total && quantity <= product.getStock() && quantity != 0) {
				upStock(productID, quantity);
				BuyDomain buyProduct = new BuyDomain(productID, amount, quantity, change, total);

				BUY_MAP.put(buyID, buyProduct);
			} else {
				throw new RuntimeException("ใส่จำนวนสินค้าและจำนวนเงินที่ต้องการซื้อให้ถูกต้องด้วยครับ");
			}
		} else {
			throw new RuntimeException(productID + " : ไม่พบสินค้าที่ท่านต้องการ");
		}
		return BUY_MAP.get(buyID);
	}

	public static void upStock(String productID, int quantity) {

		ProductDomain upProduct = PRODUCT_MAP.get(productID);
		int upDate = upProduct.getStock() - quantity;
		upProduct.setStock(upDate);
		PRODUCT_MAP.put(upProduct.getProductID(), upProduct);

	}

	public List<BuyDomain> history() {
//		Integer totalPrice = 0;
//		System.out.println("----- Purchase History ----- ");
//		for (Entry<String, BuyDomain> entry : BUY_MAP.entrySet()) {
//			totalPrice += entry.getValue().getTotal();
//			
////			BuyDomain list = entry.getValue();
////			ProductDomain product = PRODUCT_MAP.get(entry.getValue().getProductID());
////			System.out.println(entry.getKey() + " " + " Amount = " + entry.getValue().getAmount().toString() + " ฿"
////					+ ", Quantity " + list.getQuantity() + ", Price : " + product.getPrice().toString() + " ฿/unit "
////					+ ", Change " + list.getChange() + " ฿ " + ", Total " + list.getTotal() + " ฿ ");
//		}System.out.println("TotalPrice = " + totalPrice + " ฿ ");
		List<BuyDomain> listHistory = new ArrayList<>();
		BUY_MAP.entrySet().stream().forEach(domain -> {
			listHistory.add(domain.getValue());
		});
		
		
		return listHistory;
	}

}

package com.plustitsolution.zeenshop.domain;

public class BuyDomain {
	private String productID;
	private Integer amount;
	private Integer quantity;
	private Integer change;
	private Integer total;
	
	
	
	
	
	
	
	
	public BuyDomain(String productID, Integer amount, Integer quantity, Integer change, Integer total) {
		super();
		this.productID = productID;
		this.amount = amount;
		this.quantity = quantity;
		this.change = change;
		this.total = total;
	}
	
	
	
	public Integer getChange() {
		return change;
	}



	public void setChange(Integer change) {
		this.change = change;
	}



	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}



	@Override
	public String toString() {
		return " Buy " + productID + ", amount = " + amount + ", quantity = " + quantity + ", change = "
				+ change + ", total = " + total + "]";
	}
	
	
	
	
}

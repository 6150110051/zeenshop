package com.plustitsolution.zeenshop.domain;

public class ProductDomain {
	private String productID;
	private Integer price;
	private Integer stock;
	
	
	
	
	
	
	
	public ProductDomain(String productID, Integer price, Integer stock) {
		super();
		this.productID = productID;
		this.price = price;
		this.stock = stock;
	}

	
	
	public String getProductID() {
		return productID;
	}



	public void setProductID(String productID) {
		this.productID = productID;
	}



	public Integer getPrice() {
		return price;
	}



	public void setPrice(Integer price) {
		this.price = price;
	}



	public Integer getStock() {
		return stock;
	}



	public void setStock(Integer stock) {
		this.stock = stock;
	}



	@Override
	public String toString() {
		return "ProductDomain [productID=" + productID + ", price=" + price + ", stock=" + stock + "]";
	}



	
	
}
